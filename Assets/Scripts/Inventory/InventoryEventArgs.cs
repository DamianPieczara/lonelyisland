using System;

public class InventoryEventArgs : EventArgs
{
    public IInventoryItem Item { get; set; }

    public InventoryEventArgs(IInventoryItem item)
    {
        Item = item;
    }
}
