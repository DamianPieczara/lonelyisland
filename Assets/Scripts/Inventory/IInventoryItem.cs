using UnityEngine;

public interface IInventoryItem
{
    string Name { get; }
    Sprite Image { get; }

    Vector3 PickUpPosition { get; }
    Vector3 PickUpRotation { get; }

    void OnPickup();
    void OnUse(IInventoryItem currentItem);
}
