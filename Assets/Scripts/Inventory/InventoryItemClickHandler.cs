﻿using UnityEngine;

public class InventoryItemClickHandler : MonoBehaviour
{
    public Inventory Inventory;

    public void OnItemClick()
    {
        var clickedItem = gameObject.transform.Find(Constants.Item).GetComponent<InventoryItemHolder>().Item;
        
        clickedItem?.OnUse(Inventory.CurrentItem);

        Inventory.CurrentItem = clickedItem;
    }
}
