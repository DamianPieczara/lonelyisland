﻿using UnityEngine;

public class InventoryItemPickUpHandler : MonoBehaviour
{
    public float PickUpRange;
    public float DropForwardForce, DropUpwardForce;

    public bool IsEquipped;
    public bool IsInInventory;

    public BoxCollider WeaponBoxCollider;
    public Rigidbody WeaponRigidbody;
    public Transform HeroTransform, HeroRightHand, CameraTransform;
    public Inventory Inventory;

    void Start()
    {
        if (IsEquipped)
        {
            WeaponRigidbody.isKinematic = true;
            WeaponBoxCollider.isTrigger = true;
        }
        else
        {
            WeaponRigidbody.isKinematic = false;
            WeaponBoxCollider.isTrigger = false;
        }
    }

    void Update()
    {
        var distanceToPlayer = HeroTransform.position - transform.position;

        if (!PauseMenu.IsPaused && !Inventory.IsOpened)
        {
            if (!IsInInventory && !IsEquipped && distanceToPlayer.magnitude <= PickUpRange && Input.GetKeyDown(KeyCode.E))
            {
                PickUp();
            }

            if (IsInInventory && IsEquipped && Input.GetKeyDown(KeyCode.Q))
            {
                Drop();
            }
        }
    }

    private void PickUp()
    {
        IsInInventory = true;

        var pickedUpItem = gameObject.GetComponent<IInventoryItem>();

        Inventory.AddItem(pickedUpItem);

        var itemGameObject = (pickedUpItem as MonoBehaviour).gameObject;

        itemGameObject.transform.parent = HeroRightHand;

        itemGameObject.transform.localPosition = pickedUpItem.PickUpPosition;
        itemGameObject.transform.localEulerAngles = pickedUpItem.PickUpRotation;
        itemGameObject.transform.localScale = Vector3.one;

        WeaponRigidbody.isKinematic = true;
        WeaponBoxCollider.isTrigger = true;
    }
    
    private void Drop()
    {
        IsEquipped = false;

        IsInInventory = false;

        Inventory.HasBusySlot = false;

        var droppedItem = gameObject.GetComponent<IInventoryItem>();

        Inventory.RemoveItem(droppedItem);

        if (Inventory.CurrentItem.Equals(droppedItem))
        {
            Inventory.CurrentItem = null;
        }

        transform.parent = null;

        WeaponRigidbody.isKinematic = false;
        WeaponBoxCollider.isTrigger = false;

        WeaponRigidbody.velocity = HeroTransform.GetComponent<Rigidbody>().velocity;

        WeaponRigidbody.AddForce(CameraTransform.forward * DropForwardForce, ForceMode.Impulse);
        WeaponRigidbody.AddForce(CameraTransform.up * DropUpwardForce, ForceMode.Impulse);

        var random = Random.Range(-1f, 1f);

        WeaponRigidbody.AddTorque(new Vector3(random, random, random) * 10);
    }
}
