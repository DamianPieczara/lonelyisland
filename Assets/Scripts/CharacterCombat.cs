﻿using System;
using System.Collections;
using UnityEngine;

[RequireComponent(typeof(CharacterStats))]
public class CharacterCombat : MonoBehaviour
{
    private float _attackCooldown;

    private CharacterStats _characterStats;
    
    public float AttackSpeed;
    public float AttackDelay;

    public event Action OnAttack;

    public CharacterCombat()
    {
        _attackCooldown = 0f;

        AttackSpeed = 0.5f;
        AttackDelay = 0.6f;
    }
    
    void Start()
    {
        _characterStats = GetComponent<CharacterStats>();
    }

    void Update()
    {
        _attackCooldown -= Time.deltaTime;
    }

    public void Attack(CharacterStats enemyStats)
    {
        if (_attackCooldown <= 0f)
        {
            StartCoroutine(DealDamage(enemyStats, AttackDelay));

            OnAttack?.Invoke();

            _attackCooldown = 1f / AttackSpeed;
        }

    }

    private IEnumerator DealDamage(CharacterStats stats, float delay)
    {
        yield return new WaitForSeconds(delay);

        stats.TakeDamage(_characterStats.Damage);
    }
}
