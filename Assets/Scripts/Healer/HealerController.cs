﻿using UnityEngine.AI;
using UnityEngine;

public class HealerController : MonoBehaviour
{
    [SerializeField] private float lookRadius = 3f;
    [SerializeField] private float minimalPeriod = 30;
    private Transform target;
    private AudioSource[] audioSource;
    private CharacterStats targetStats;
    private float lastTime = 0;


    void Awake()
    {
        target = GameObject.Find("Hero").transform;
        targetStats = target.GetComponent<CharacterStats>();
        audioSource = GetComponents<AudioSource>();
    }

    void Update()
    {
        float distance = Vector3.Distance(target.position, transform.position);

        if(distance <= lookRadius)
        {
            if(Input.GetKeyDown(KeyCode.L) && targetStats.CurrentHealth < 100.0f)
            {
                if((Time.time - lastTime ) > minimalPeriod)
                {
                    Heal();
                    lastTime = Time.time;
                }
                else
                {
                    Debug.Log("\nOstatni raz: " + lastTime.ToString());
                    Debug.Log("Uplynelo: " + (Time.time - lastTime).ToString());
                    Debug.Log("Pozostało: " + Mathf.Abs(((Time.time - lastTime) - minimalPeriod)).ToString());
                }
            }

            FaceTarget();
        }
    }

    void Heal()
    {
        targetStats.CurrentHealth = targetStats.Health;
        audioSource[0].Play();
        audioSource[1].Play();
    }

    void FaceTarget()
    {
        Vector3 direction = ( target.position - transform.position ).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere(transform.position, lookRadius);
    }
}
