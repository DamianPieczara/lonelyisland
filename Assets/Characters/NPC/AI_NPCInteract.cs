﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_NPCInteract : MonoBehaviour
{
        public float radius = 3f;

        bool isFocus = false;
        bool hasInteracted = false;
        Transform player;


        public virtual void Interact()
        {
                // This method is meant to be overwritten 
                Debug.Log("Interacting with " + transform.name);
        }
        public void OnFocused(Transform playerTransform)
        {
                isFocus = true;
                player = playerTransform;
                hasInteracted = false;
        }

        public void OnDefocused()
        {
                isFocus = false;
                player = null;
                hasInteracted = false;
        }

        void Update()
        {
                if (isFocus && !hasInteracted)
                {
                        float distance = Vector3.Distance(player.position, transform.position);
                        if (distance <= radius)
                        {
                                Interact();
                                hasInteracted = true;
                        }
                }
        }
        void OnDrawGizmosSelected()
        {
                Gizmos.color = Color.yellow;
                Gizmos.DrawWireSphere(transform.position, radius);
        }
}
